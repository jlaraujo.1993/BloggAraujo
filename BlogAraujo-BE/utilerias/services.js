var jwt = require('jwt-simple');
var moment = require('moment');
const config = require('../config');


exports.createToken = function(user) {
  var payload = {
    sub: user.Nombre,
    iat: moment().unix(),
    exp: moment().add(1, "days").unix(),
  };
  return jwt.encode(payload, config.TOKEN_SECRET);
};