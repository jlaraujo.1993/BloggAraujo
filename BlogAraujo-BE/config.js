module.exports = {
	port: process.env.PORT || 9001,
	db: process.env.MONGODB || 'mongodb://localhost:27017/BlogAraujo',
	TOKEN_SECRET: process.env.TOKEN_SECRET || "tokenultrasecreto",
	portWebSocket: process.env.PORTWEBSOCKET || "1337"
}