'use strict'

const express = require('express');
const usuarioCtrl = require('../controladores/usuario'); 
const seguridadCtrl = require('../controladores/seguridad'); 
const publicacionesCtrl = require('../controladores/publicaciones'); 
const api = express.Router();


//Publicaciones
api.post('/publicaciones', publicacionesCtrl.Guardar);
api.get('/publicaciones', publicacionesCtrl.Consultar);
api.put('/publicaciones/:publicacionid', publicacionesCtrl.AgregarComentario);
api.put('/publicaciones/megusta/:publicacionid', publicacionesCtrl.MeGusta);
api.put('/publicaciones/nomegusta/:publicacionid', publicacionesCtrl.NoMeGusta);
api.get('/publicaciones/:publicacionid',publicacionesCtrl.ConsultarId);
api.get('/publicaciones/categoria/:categoria',publicacionesCtrl.ConsultarPorCategoria);

//Usuarios
api.get('/usuario', usuarioCtrl.Consultar);
api.post('/usuario/validarcorreo', usuarioCtrl.ValidarCorreo);
api.put('/usuario', usuarioCtrl.ActualizarAvatar);
api.post('/usuario', usuarioCtrl.Guardar);

//Sesion
api.post('/seguridad/iniciarsesion', seguridadCtrl.IniciarSesion);
api.post('/seguridad/cerrarsesion', seguridadCtrl.CerarSesion);
api.post('/seguridad/verificatoken', seguridadCtrl.VerificaToken);
api.post('/seguridad/login', seguridadCtrl.Login);

module.exports = api;