'use strict'

const Publicaciones = require('../modelos/publicaciones');
const Usuarios = require('../modelos/usuario');
const moment = require('moment');
 
function Guardar(req, res){
    let publicaciones = new Publicaciones();
    publicaciones.TituloPublicacion =  req.body.TituloPublicacion;
    publicaciones.Descripcion = req.body.Descripcion;
    publicaciones.Comentarios = req.body.Comentarios;
    publicaciones.Vistos = req.body.Vistos;
    publicaciones.MeGusta = req.body.MeGusta;
    publicaciones.NoMeGusta = req.body.NoMeGusta;
    publicaciones.Fecha = moment().format('YYYY-MM-DD HH:mm:ss');
    publicaciones.Correo = req.body.Correo;
    publicaciones.Categoria = req.body.Categoria;

    publicaciones.save((err, publicacionesStored) => {
        if(err)
            Respuesta(res,500,'Error al guardar en la base de datos.' + err, null);
        Respuesta(res, 200,'Publicacíon registrada con exito', publicacionesStored);
    });
}

function Consultar (req, res){
        Publicaciones.find({}, function (err,publicacionesStored){
            if(err) {
                Respuesta(res,500,'Error al realizar la petición.' + err, null); 
            } else if(!publicacionesStored) {
                Respuesta(res,404,'No existen publicaciones.', null); 
            } else{
                var objPubli = []; 
                var i = 0;
                publicacionesStored.forEach( f => {
                    Usuarios.findOne({Correo: f.Correo}, function(err, usuarioStored){ 
                        let publi = {
                            _id : f._id,
                            TituloPublicacion : f.TituloPublicacion,
                            Descripcion : f.Descripcion,
                            Comentarios : f.Comentarios,
                            Vistos : f.Vistos,
                            MeGusta : f.MeGusta,
                            NoMeGusta : f.NoMeGusta,
                            Fecha : f.Fecha,
                            Correo : f.Correo,
                            Categoria : f.Categoria,
                            Usuario: {
                                Nombre: usuarioStored.Nombre,
                                Apellido: usuarioStored.Apellido,
                                Avatar: usuarioStored.Avatar
                            } 
                        };
                        i++;
                        objPubli.push(publi);
                        if(i == publicacionesStored.length){ 
                            Respuesta(res,200,'', objPubli.sort(sortFunction));
                        }
                    });
                }); 
            }
        }); 
} 
function MeGusta(req, res){ 
    Publicaciones.update({ _id: req.params.publicacionid}, 
    { 
        $inc: { 
            MeGusta : 1
        }        
    }, function(errr, docc){
        if(errr){
            Respuesta(res,400,'Error' + errr, null);
        } else {       
            Respuesta(res,200,'', null);
        }
    }); 
} 
function NoMeGusta(req, res){ 
    Publicaciones.update({ _id: req.params.publicacionid}, 
    { 
        $inc: { 
            NoMeGusta : 1
        }        
    }, function(errr, docc){
        if(errr){
            Respuesta(res,400,'Error' + errr, null);
        } else {       
            Respuesta(res,200,'', null);	
        }
    }); 
} 

function ConsultarPorCategoria (req, res){
    Publicaciones.find({Categoria:  req.params.categoria}, function (err,publicacionesStored){
        if(err) {
            Respuesta(res,500,'Error al realizar la petición.' + err, null); 
        } else if(!publicacionesStored) {
            Respuesta(res,404,'No existen publicaciones.', null); 
        } else{ 
            var objPubli = []; 
            var i = 0;
            publicacionesStored.forEach( f => {
                Usuarios.findOne({Correo: f.Correo}, function(err, usuarioStored){ 
                    let publi = {
                        _id : f._id,
                        TituloPublicacion : f.TituloPublicacion,
                        Descripcion : f.Descripcion,
                        Comentarios : f.Comentarios,
                        Vistos : f.Vistos,
                        MeGusta : f.MeGusta,
                        NoMeGusta : f.NoMeGusta,
                        Fecha : f.Fecha,
                        Correo : f.Correo,
                        Categoria : f.Categoria,
                        Usuario: {
                            Nombre: usuarioStored.Nombre,
                            Apellido: usuarioStored.Apellido,
                            Avatar: usuarioStored.Avatar
                        } 
                    };
                    i++;
                    objPubli.push(publi);
                    if(i == publicacionesStored.length){ 
                        Respuesta(res,200,'', objPubli.sort(sortFunction));
                    }
                });
            }); 
        }
    }); 
} 



function sortFunction(a,b){  
    var dateA = new Date(a.Fecha).getTime();
    var dateB = new Date(b.Fecha).getTime();
    return dateA > dateB ? -1 : 1;  
};

function AgregarComentario(req, res){    
    Publicaciones.update({ _id: req.params.publicacionid}, 
    { 
        $addToSet: { 
            ListadoComentarios: {
                Descripcion: req.body.Descripcion,
                Correo: req.body.Correo,
                MeGusta: req.body.MeGusta,
                NoMeGusta: req.body.NoMeGusta,
                Fecha: moment().format('YYYY-MM-DD HH:mm:ss')
            }
        }       
    }, function(err, doc){
        if(err){
            Respuesta(res,400,'Error' + err, null);
        } else {
            Publicaciones.update({ _id: req.params.publicacionid}, 
            { 
                $inc: { 
                    Comentarios : 1
                }        
            }, function(errr, docc){
                if(errr){
                    Respuesta(res,400,'Error' + errr, null);
                } else {
                    
                    Publicaciones.findById(req.params.publicacionid, (errr, publicacionesStored) => {
                        Respuesta(res,200,'Comentario agregado con exito.', publicacionesStored);
                    });	
                }
            });
        }
    });
} 

function ConsultarId (req, res){
    let publicacionId = req.params.publicacionid;
    Publicaciones.update({ _id: publicacionId}, 
        { 
            $inc: { 
                Vistos : 1
            }        
        }, function(errr, docc){
            if(errr){
                Respuesta(res,400,'Error' + errr, null);
            } else { 
                Publicaciones.findById(publicacionId, (err, publicacionesStored) => {
                    if(err){
                        Respuesta(res,400,'Error' + err, null);
                    } else if(!publicacionesStored) {
                            Respuesta(res,404,'No existen la publicaciones.', null); 
                    } else {
                        Usuarios.findOne({Correo: publicacionesStored.Correo}, function(err, usuarioStored){ 
                            let publicaciones = { 
                                _id: publicacionesStored._id,
                                TituloPublicacion: publicacionesStored.TituloPublicacion,
                                Descripcion: publicacionesStored.Descripcion,
                                Comentarios: publicacionesStored.Comentarios,
                                Vistos: publicacionesStored.Vistos,
                                MeGusta: publicacionesStored.MeGusta,
                                NoMeGusta: publicacionesStored.NoMeGusta,
                                Fecha: publicacionesStored.Fecha,
                                Correo: publicacionesStored.Correo,
                                Categoria: publicacionesStored.Categoria,
                                ListadoComentarios:publicacionesStored.ListadoComentarios,
                                Usuario: {
                                    Nombre: usuarioStored.Nombre,
                                    Apellido: usuarioStored.Apellido,
                                    Avatar: usuarioStored.Avatar
                                }
                            };
                            if (publicaciones.ListadoComentarios.length != 0){            
                                var objListaComent = []; 
                                var i = 0;
                                publicaciones.ListadoComentarios.forEach( f => {
                                    Usuarios.findOne({Correo: f.Correo}, function(err, usuarioStored){ 
                                        let Coment = { 
                                            Descripcion: f.Descripcion,
                                            Correo: f.Correo,
                                            MeGusta: f.MeGusta,
                                            NoMeGusta: f.NoMeGusta,
                                            Fecha: f.Fecha,
                                            Nombre: usuarioStored.Nombre,
                                            Apellido: usuarioStored.Apellido,
                                            Avatar: usuarioStored.Avatar
                                        };
                                        i++;
                                        objListaComent.push(Coment);
                                        if(i == publicaciones.ListadoComentarios.length){ 
                                            publicaciones.ListadoComentarios = objListaComent.sort(sortFunction)
                                            Respuesta(res,200,'', publicaciones);
                                        }
                                    });
                                }); 
                            }
                            else {
                                Respuesta(res,200,'', publicaciones);            
                            }
                        });
                    }
                });
            }
        });
}
function Respuesta (res,estatus,mensaje,respuesta){
	let objRespuesta = {
		Estatus :estatus,
		Mensaje: mensaje,
		Respuesta: respuesta
	}
	return res.status(estatus).send(objRespuesta);
}
module.exports = {
    Guardar,
    Consultar,
    AgregarComentario,
    ConsultarId,
    ConsultarPorCategoria,
    MeGusta,
    NoMeGusta
}