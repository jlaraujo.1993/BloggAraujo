'use strict'

const Usuario = require('../modelos/usuario');

function Consultar (req, res){
	Usuario.find({}, (err,usuarios) =>{
		if(err) 
			Respuesta(res,500,'Error al realizar la petición.' + err, null); 
		else if(!usuarios) 
			Respuesta(res,404,'No existe un usuario.', null); 
		else
			Respuesta(res, 200,'', usuarios);
	});
}

function Guardar(req, res){	  
	Usuario.findOne({Correo: req.body.Correo}, function(err, valusuarioStored){
		if(valusuarioStored != null){
			Respuesta(res,409,'No permitido, el usuario ya existe', null);
		} else  {
			let usuario = new Usuario();
			usuario.Avatar = req.body.Avatar;
			usuario.Nombre = req.body.Nombre;
			usuario.Apellido = req.body.Apellido;
			usuario.Correo = req.body.Correo;
			usuario.Contrasena = req.body.Contrasena;

			usuario.save((err, usuarioStored) => {
				if(err)
					Respuesta(res,500,'Error al guardar en la base de datos.' + err, null);
				Respuesta(res, 200,'Usuario registrado con exito', usuarioStored);
			});
		}
	});
}

function ActualizarAvatar(req, res){
	Usuario.findOne({Correo: req.body.Correo}, function(err, usuarioStored){ 
		if(usuarioStored == null){
			Respuesta(res,404,'No existe el usuario.', null);
		}else {
			Usuario.update({ _id: usuarioStored._id}, {"$set":{Avatar:  req.body.Avatar}}, function(err, doc){
				if(err){
					Respuesta(res,400,'Error' + err, null);
				} else {
					let obj = {
						Correo: req.body.Correo,
						Avatar: req.body.Avatar
					};
					Respuesta(res,200,'Imagen actualizada', obj);
				}
			});
		}
	});
}

function ValidarCorreo(req, res){
	Usuario.findOne({Correo: req.body.Correo}, function(err, usuarioStored){
		if(usuarioStored == null){
			Respuesta(res,404,'No existe el usuario.', null);
		}else {
			let obj = {
				Avatar: usuarioStored.Avatar,
				Nombre: usuarioStored.Nombre,
				Apellido: usuarioStored.Apellido
			};
			Respuesta(res,200,'', obj); 
		}
	});
}

function Respuesta (res,estatus,mensaje,respuesta){
	let objRespuesta = {
		Estatus :estatus,
		Mensaje: mensaje,
		Respuesta: respuesta
	}
	return res.status(estatus).send(objRespuesta);
}

module.exports = {
	Guardar,
	Consultar,
	ActualizarAvatar,
	ValidarCorreo
}