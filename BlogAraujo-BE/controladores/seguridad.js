'use strict'

const Sesion = require('../modelos/sesion');
const Usuario = require('../modelos/usuario');
var service = require('../utilerias/services');

function VerificaToken(req, res){
	Sesion.findOne({Token: req.body.Token, EsActivo: true}, function(err, sesionStored){
		if(sesionStored == null){
			Respuesta(res,404,'No existe el token.', null);
		} else { 
			Usuario.findOne({Correo: sesionStored.Correo}, function(err, usuarioStored){
				if(usuarioStored == null){
					Respuesta(res,404,'No se encontro al usuario.', null);
				}else {  
					let obj = {
						_id: sesionStored._id,
						Nombre: sesionStored.Nombre,
						Correo: sesionStored.Correo,
						Token: sesionStored.Token,
						Avatar: usuarioStored.Avatar
					}
					Respuesta(res, 200, '', obj);}
			});
		}
	});
}

function CerarSesion(req, res){
	Sesion.findOne({Token: req.body.Token, EsActivo: true}, function(err, sesionStored){
		if(sesionStored == null){
			Respuesta(res,404,'No existe el token.', null);
		}else {
			Sesion.update({ _id: sesionStored._id}, {"$set":{EsActivo: false}}, function(err, doc){
				if(err){
					Respuesta(res,400,'Error', null);
				} else {
					Respuesta(res,200,'Sesion cerrada', null);
				}
			});
		}
	});
}

function Login(req, res){
	Usuario.findOne({Correo: req.body.Correo, Contrasena: req.body.Contrasena}, function(err, usuarioStored){
		if(usuarioStored == null){
			Respuesta(res,404,'Datos incorrectos.', null);
		}else { 
			let sesion = new Sesion();
			sesion.Nombre = usuarioStored.Nombre;
			sesion.Correo = usuarioStored.Correo;
			sesion.Token = service.createToken(sesion);
			sesion.EsActivo = true;
			sesion.FechaCreacion = new Date();

			sesion.save((err, sesionStored) => {
				if(err){
					Respuesta(res,500,'Error al guardar en la base de datos.' + err, null);
				} else {
					let obj = {
						Nombre: sesionStored.Nombre,
						Correo: sesionStored.Correo,
						Token: sesionStored.Token,
						Avatar: usuarioStored.Avatar
					}
					Respuesta(res, 200,'Bienvenido', obj);
				}
			});
		}
	});
}

function IniciarSesion(req, res){
	let sesion = new Sesion();
	sesion.Nombre = req.body.Nombre;
	sesion.Correo = req.body.Correo;
	sesion.Token = service.createToken(sesion);
	sesion.EsActivo = true;
	sesion.FechaCreacion = new Date();

	sesion.save((err, sesionStored) => {
		if(err)
			Respuesta(res,500,'Error al guardar en la base de datos.' + err, null);
		else
			Respuesta(res, 200,'Sesion Registrado con exito', sesionStored);
	});
}

function Respuesta (res,estatus,mensaje,respuesta){
	let objRespuesta = {
		Estatus :estatus,
		Mensaje: mensaje,
		Respuesta: respuesta
	}
	return res.status(estatus).send(objRespuesta);
}

module.exports = {
	IniciarSesion,
	VerificaToken,
	CerarSesion,
	Login
}