const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UsuarioSchema = Schema({
	Avatar: String,
	Nombre: String,
	Apellido: String,
	Correo: String,
	Contrasena: String
});

module.exports = mongoose.model('Usuario', UsuarioSchema);