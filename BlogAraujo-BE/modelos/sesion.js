const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SesionSchema = Schema({
    Token: String,
    Nombre: String,
    Correo: String,
    EsActivo: Boolean,
    FechaCreacion : Date
});

module.exports = mongoose.model('Sesion', SesionSchema);