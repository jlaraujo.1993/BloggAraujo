const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moment = require('moment');


const PublicacionesSchema = Schema({
    TituloPublicacion : String,
    Descripcion : String,
    Comentarios : Number,
    Vistos : Number,
    MeGusta : Number,
    NoMeGusta : Number, 
    Fecha : String,
    Correo : String,
    Categoria : String,
    ListadoComentarios:[]
});

module.exports = mongoose.model('Publicaciones', PublicacionesSchema);