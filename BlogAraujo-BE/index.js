'use strict' 
const mongoose= require('mongoose');
const app = require('./server'); 
const config = require('./config');



mongoose.connect(config.db, (err, res) => {
	if(err) { 
		return console.log("Error al conectar a la base de datos: " + err);
	} else {
        console.log('Conexion a la base de datos establecida...');
        app.listen(config.port, () =>{
            console.log("Http:\localhost:" + config.port);
        });
    }
});


var WebSocketServer = require('websocket').server;
var http = require('http');

var server = http.createServer(function(request, response){

});
server.listen(config.portWebSocket, (err, res) => {
    if(err) { 
		return console.log("Error al levantar el socket: " + err);
	} else {
        console.log("Servicio Socket activo...");
        console.log("Por el puerto Http:\\localhost:" + config.portWebSocket);
    }
});
 

var WsServer = new WebSocketServer({
    httpServer: server
});
var conexiones = [];
WsServer.on('request', function(req){
    var conection = req.accept(null, req.origin+req.resource); 
    conexiones.push(conection);
    conection.on('message', function(msg){
        if(msg.type === 'utf8'){ 
            conexiones.forEach(function(cnx){
                cnx.send(msg.utf8Data);        
            });        
        }
    });
    conection.on('close', function(cnx){
        //console.log('conexion cerrada');
    });
});