import { Usuarios } from '../modelos/usuarios/Usuarios';
import { EnumVistas } from '../enumeradores/enum-vistas';

export class MostrarQuitarMenuDerecho {
    constructor(public Mostrar: boolean, public vista: EnumVistas) {}
}
export class QuitarMenuDerechoUsuarioLogeado {
    constructor() {}
}
export class TokenValido {
    constructor() {}
}
export class TokenNoValido {
    constructor() {}
}
export class MostrarMensaje {
    constructor(public claseAlerta: string, public mensaje: string) { }
}
export class VistaTrabajo {
    constructor(public vista: EnumVistas) { }
}
export class IniciarSesion {
    constructor( public usuario: Usuarios) {}
}
export class Cerrarsesion {
    constructor() {}
}
export class SesionEstablecidaEvent {
    constructor(public valor: number) { }
}
export class ServicioSocket {
    constructor(public _id: any) { }
}