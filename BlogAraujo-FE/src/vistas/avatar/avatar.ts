import {autoinject} from "aurelia-framework";
import { DialogController } from 'aurelia-dialog';


@autoinject
export class Avatar { 
    tituloModal: string = "";
    constructor(public controller: DialogController) {
        controller.settings.keyboard = true;
    } 
}