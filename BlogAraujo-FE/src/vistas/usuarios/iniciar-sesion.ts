import { autoinject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import * as eventos from '../../eventos/eventos';
import { Usuarios } from '../../modelos/usuarios/Usuarios';
import { ApiRespuesta } from '../../servicios/api-respuesta';
import { EnumRespuestaAPI } from "../../enumeradores/enum-respuesta-api"; ;

@autoinject
export class IniciarSesion {
    RegresarBind : boolean = false;
    AvanzarBind : boolean = true;
    AceptarBind : boolean = false;
    focusCorreoBind: boolean = true;
    focusContrasenaBind: boolean = false;
    Avatar: string = '';
    Nombre: string='';
    constructor(private ea : EventAggregator, private usuarios : Usuarios, private apiRespuesta: ApiRespuesta){
        usuarios.Correo = '';
    }
    IniciarSesion(){  
        this.usuarios.IniciarSesion()
        .then(respuesta => {
            this.apiRespuesta.ProcesarRespuesta(respuesta);           
        });
    }
    Avanzar(){
        this.usuarios.ValidarCorreo()
        .then(respuesta => {
            console.log(3);
            this.apiRespuesta.ProcesarRespuesta(respuesta); 
            if (respuesta.Estatus == EnumRespuestaAPI.Aceptado){
                this.AvanzarBind = false;
                this.RegresarBind = true;
                this.focusContrasenaBind = true;
                this.usuarios.Contrasena = '';
                this.AceptarBind = true; 
                this.Avatar = respuesta.Respuesta.Avatar;
                this.Nombre = respuesta.Respuesta.Nombre + ' ' + respuesta.Respuesta.Apellido;
            }
        });
        
    }
    Regresar(){
        this.AvanzarBind = true;
        this.RegresarBind = false;
        this.focusCorreoBind = true;
        this.AceptarBind = false;
        this.Avatar = '';
        this.Nombre = '';

    }
}