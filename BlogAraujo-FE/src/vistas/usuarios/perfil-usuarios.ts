import { autoinject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { DialogService } from 'aurelia-dialog';
import * as eventos from '../../eventos/eventos';
import { Usuarios } from '../../modelos/usuarios/Usuarios';
import { ApiRespuesta } from '../../servicios/api-respuesta';
import { Sesion } from "../../modelos/seguridad/sesion";
import { EnumRespuestaAPI } from "../../enumeradores/enum-respuesta-api";
import { Avatar } from '../avatar/avatar';
import { Publicaciones } from '../../modelos/publicaciones/publicaciones';



@autoinject
export class PerfilUsuarios{
    CambioContrasena: boolean = false;
    Cancelar: boolean = false;
   
    constructor(private publicaciones: Publicaciones, private ea : EventAggregator,private sesion: Sesion, private usuarios : Usuarios, private apiRespuesta: ApiRespuesta, public dlg: DialogService){
    
    }
    CambiarImagen(){
        this.dlg.open({ viewModel: Avatar, model: "", lock: false }).whenClosed(respuesta => {
            if (!respuesta.wasCancelled){
               this.usuarios.Avatar = respuesta.output;
               this.usuarios.ActualizarAvatar()
               .then(respuest => {
                   console.log(this.sesion);
                    this.apiRespuesta.ProcesarRespuesta(respuest);
                    if (respuest.Estatus == EnumRespuestaAPI.Aceptado){
                        this.sesion.Avatar = respuesta.output;
                        this.publicaciones.ConsultarPublicaciones();
                    }
                }); 
            }
        });
    }

    cambiarContrasena(){
        this.CambioContrasena = true;
        this.Cancelar = true;
    }
    CancelarPeticiones(){
        if(this.CambioContrasena){
            this.CambioContrasena = false;
            this.Cancelar = false;
        }
    }
    cerrarSesion(){
        this.ea.publish(new eventos.Cerrarsesion());
    }
}