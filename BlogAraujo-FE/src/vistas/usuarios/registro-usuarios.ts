import { autoinject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { DialogService } from 'aurelia-dialog';
import * as eventos from '../../eventos/eventos';
import { Usuarios } from '../../modelos/usuarios/Usuarios';
import { ApiRespuesta } from '../../servicios/api-respuesta';
import { EnumRespuestaAPI } from "../../enumeradores/enum-respuesta-api"; ;
import { Avatar } from '../avatar/avatar';


@autoinject
export class RegistroUsuarios{
 
    focusNombreBind: boolean = true;
    constructor(private ea : EventAggregator, private usuarios : Usuarios, private apiRespuesta: ApiRespuesta, public dlg: DialogService){
        usuarios.Correo = '';
    }
    Guardar(){  
        this.usuarios.Guardar()
        .then(respuesta => {
            this.apiRespuesta.ProcesarRespuesta(respuesta);
            if (respuesta.Estatus == EnumRespuestaAPI.Aceptado){
                this.ea.publish(new eventos.QuitarMenuDerechoUsuarioLogeado());
                this.ea.publish(new eventos.IniciarSesion(respuesta.Respuesta));
            }
        }); 
    }

    CambiarImagen(){
        this.dlg.open({ viewModel: Avatar, model: "", lock: false }).whenClosed(respuesta => {
            if (!respuesta.wasCancelled) 
               this.usuarios.Avatar =respuesta.output;
        });
    }
}