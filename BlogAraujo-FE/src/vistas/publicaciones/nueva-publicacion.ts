import { autoinject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import * as eventos from '../../eventos/eventos';
import { EnumVistas } from '../../enumeradores/enum-vistas';
import { Publicaciones } from '../../modelos/publicaciones/publicaciones';
import { ApiRespuesta } from '../../servicios/api-respuesta';
import { EnumRespuestaAPI } from "../../enumeradores/enum-respuesta-api"; ;
import { ApiWebSocket } from '../../servicios/webapi/api-websocket';


@autoinject
export class AltaPublicaciones  {
    constructor(private ea: EventAggregator, private publicaciones: Publicaciones, private apiRespuesta: ApiRespuesta, private apiWebSock: ApiWebSocket){
        publicaciones.Categoria = "Seleccione";
        publicaciones.TituloPublicacion = "";
        publicaciones.Descripcion = "";
    }
    Guardar(){
        

        this.publicaciones.GuardarPublicacion()
        .then(respuesta => {
            this.apiRespuesta.ProcesarRespuesta(respuesta);
            if (respuesta.Estatus == EnumRespuestaAPI.Aceptado){
                this.apiWebSock.EnviarMensaje(respuesta.Respuesta._id);
                this.ea.publish(new eventos.VistaTrabajo(EnumVistas.vistaPublicaciones));
            }
        }); 
    }
}