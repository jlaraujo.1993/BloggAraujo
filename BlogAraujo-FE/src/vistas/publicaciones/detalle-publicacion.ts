import { autoinject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import * as eventos from '../../eventos/eventos';
import { EnumVistas } from '../../enumeradores/enum-vistas';
import { Publicaciones } from '../../modelos/publicaciones/publicaciones';


 
import { ApiRespuesta } from '../../servicios/api-respuesta';
import { EnumRespuestaAPI } from "../../enumeradores/enum-respuesta-api"; ;


@autoinject
export class DetallePublicacion{
    MostrarComentario: boolean = false;
    ComentarioFocus: boolean = false;
    constructor(private ea: EventAggregator, private publicaciones: Publicaciones,  private apiRespuesta: ApiRespuesta){
        this.publicaciones.ConsultarPorId();
    }
    
    ActivarComentario(){
        this.ComentarioFocus = !this.ComentarioFocus;
        this.publicaciones.Comentario.Descripcion = "";
        this.MostrarComentario = !this.MostrarComentario;
    }

    AgregarComentario(){
        this.publicaciones.AgregarComentario()
        .then(respuesta => {
            this.apiRespuesta.ProcesarRespuesta(respuesta);
            if (respuesta.Estatus == EnumRespuestaAPI.Aceptado){
                this.MostrarComentario = false;
                this.publicaciones.ConsultarPorId(); 
            }
        }); 
    }
}

  