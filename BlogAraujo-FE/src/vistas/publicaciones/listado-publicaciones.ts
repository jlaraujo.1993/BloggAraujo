import { autoinject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import * as eventos from '../../eventos/eventos';
import { EnumVistas } from '../../enumeradores/enum-vistas';
import { Publicaciones } from '../../modelos/publicaciones/publicaciones';
import { Sesion } from '../../modelos/seguridad/sesion';



@autoinject
export class ListadoPublicaciones  {
    constructor(private ea: EventAggregator, private publicaciones: Publicaciones, private sesion: Sesion){
        if(sesion.PrimeraVes){
            publicaciones.ConsultarPublicaciones(); 
            this.sesion.PrimeraVes = false;
        }
    }
     
    DetallePublicacion(publicacionId){
        this.publicaciones._id = publicacionId;
        this.ea.publish(new eventos.VistaTrabajo(EnumVistas.vistaDetallePublicacion));
    }
}