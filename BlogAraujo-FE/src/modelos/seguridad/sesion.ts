export class Sesion{
    Token: string;
    Nombre: string;
    Correo: string;
    Avatar: string;
    PrimeraVes: boolean = true;
    constructor(){
        this.Avatar = '';
        this.Token = '';
        this.Nombre = '';
        this.Correo = '';
    }   
}