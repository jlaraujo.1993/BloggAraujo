import { autoinject } from 'aurelia-framework';
import { Router } from 'aurelia-router';
import { EventAggregator } from 'aurelia-event-aggregator';
import { Sesion } from './sesion';
import { ApiSeguridad } from '../../servicios/webapi/api-seguridad';
import ambiente from '../../environment';
import * as eventos from '../../eventos/eventos';
import { Usuarios } from '../../modelos/usuarios/Usuarios';
import { EnumRespuestaAPI } from 'enumeradores/enum-respuesta-api';
import { ApiWebSocket } from '../../servicios/webapi/api-websocket';

 @autoinject
export class Seguridad {
    usuario: Usuarios;


    constructor(private sesion: Sesion, private api: ApiSeguridad, private router: Router, private ea: EventAggregator, private apiWebSock: ApiWebSocket) { 
        this.apiWebSock.conectar();
        this.Subcripciones();
        let sToken = this.getParameterByName('stoken') ? this.getParameterByName('stoken') : '';
        if (sToken !== '') {
            let self = this;
            self.sesion.Token = sToken;
            this.api.VerificarToken()
                .then(result => { 
                    if (result.Estatus == EnumRespuestaAPI.Aceptado) {
                        self.sesion.Correo = result.Respuesta.Correo;
                        self.sesion.Nombre = result.Respuesta.Nombre;
                        self.sesion.Token = result.Respuesta.Token;
                        self.sesion.Avatar = result.Respuesta.Avatar;
                        setTimeout(() => self.ea.publish(new eventos.SesionEstablecidaEvent(1)), 500);
                    } else {
                        self.redireccionLogin();
                    }
                });
        } else {
            this.redireccionLogin();
        }
    }
    Login(obj: any){
        return new Promise<any>(results => {
            this.api.Login(obj)
            .then( result => {
                if (result.Estatus == EnumRespuestaAPI.Aceptado) {
                    let self = this;
                    this.sesion.Correo = result.Respuesta.Correo;
                    this.sesion.Nombre = result.Respuesta.Nombre;
                    this.sesion.Token =  result.Respuesta.Token;
                    this.sesion.Avatar =  result.Respuesta.Avatar;
                    setTimeout(() => self.ea.publish(new eventos.SesionEstablecidaEvent(2)), 500);
                    this.redict();
                }
                return results(result);
            });
         });
    }
    IniciarSesion(){
        this.sesion.Correo = this.usuario.Correo;
        this.sesion.Nombre = this.usuario.Nombre +' '+ this.usuario.Apellido;
        this.sesion.Avatar =  this.usuario.Avatar;
        this.api.IniciarSesion()
        .then( result => {
            this.sesion.Correo = result.Respuesta.Correo;
            this.sesion.Nombre = result.Respuesta.Nombre;
            this.sesion.Token =  result.Respuesta.Token;
            setTimeout(() => this.ea.publish(new eventos.SesionEstablecidaEvent(1)), 500);
            this.redict()
        });
    }
    CerrarSesion(){
        this.api.CerrarSesion()
        .then( result => {
            this.redireccionLogin();
        });
    }
    redict(){
        var url = ambiente.webUrl.login + '#/App?stoken='+this.sesion.Token;
        this.router.navigate(url);
    }
    redireccionLogin() {
        //Alerta de Mensaje de Usuario No Identificado
        if(location.href != ambiente.webUrl.login){
            this.router.navigate(ambiente.webUrl.login);
            this.router.navigateToRoute("redirect", { s: "mystate" }, { replace: true });
        }
    }

    getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.href);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
    Subcripciones(){
        this.ea.subscribe(eventos.IniciarSesion, res => {
            this.usuario = res.usuario;
            this.IniciarSesion();
        });
        this.ea.subscribe(eventos.Cerrarsesion, res => {
            this.CerrarSesion();
        });
    }
}
