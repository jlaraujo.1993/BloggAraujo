import { autoinject } from 'aurelia-framework';
import {ValidationRules, ValidationController} from 'aurelia-validation'; 
import {ApiUsuario} from '../../servicios/webapi/api-usuarios'
import { EnumMovimientos } from '../../enumeradores/enum-movimientos';
import { EnumValidaciones } from '../../enumeradores/enum-validaciones';
import { EnumRespuestaAPI } from "../../enumeradores/enum-respuesta-api"; 
import { Respuesta } from '../respuesta/Respuesta';
import { Seguridad } from '../../modelos/seguridad/seguridad';
import { Sesion } from '../../modelos/seguridad/sesion';


@autoinject
export class Usuarios{
    Avatar: string;
    Nombre: string;
    Apellido: string;
    Correo: string;
    Contrasena: string;
    ConfContrasena: string;
    NuevaContrasena: string;
    respuestas: Respuesta = new Respuesta;
    
    constructor(private sesion: Sesion, private validacionController: ValidationController, private peticion: ApiUsuario, private seguridad : Seguridad){
        this.InicializarVariables();
    }
    
    private InicializarVariables() {
        this.Avatar = 'imagenes/avatar.png';
        this.Nombre = "";
        this.Apellido = "";
        this.Correo = "";
        this.Contrasena = "";
        this.ConfContrasena = "";
        this.NuevaContrasena = "";
    }

    //Movimientos
    private Movimientos = {
        1: this.Alta,
        2: this.Baja,
        3: this.CambioContrasenas,
        4: this.IniciarSesionn,
        5: this.ActualizarAvatarr,
        6: this.ValidarCorreoo,
    }

    private Alta(){
       let obj = {
           Avatar: this.Avatar,
           Nombre: this.Nombre,
           Apellido: this.Apellido,
           Correo : this.Correo,
           Contrasena: this.Contrasena
       }
       return new Promise<any>(result => {
        this.peticion.Guardar(obj)
        .then(respuesta => { 
            return result(respuesta);
            });
        });
    }
    ValidarCorreoo(){
        let obj = {
            Correo : this.Correo
        }
        return new Promise<any>(result => {
         this.peticion.ValidarCorreo(obj)
         .then(respuesta => { 
             return result(respuesta);
             });
         });
    }
    ActualizarAvatarr(){
        let obj = {
            Avatar: this.Avatar,
            Correo : this.sesion.Correo,
        }
        return new Promise<any>(result => {
         this.peticion.ActualizarAvatar(obj)
         .then(respuesta => { 
             return result(respuesta);
             });
         });
    }

    private Baja(){ 
    }
    private CambioContrasenas(){ 
    }
    private IniciarSesionn(){
        let obj = {
            Correo : this.Correo,
            Contrasena: this.Contrasena
        }
        return new Promise<any>(result => {
            this.seguridad.Login(obj)
            .then(respuesta => { 
                return result(respuesta);
             });
         });
    }


    //Validaciones
    private validaciones = {
        1: this.ValidacionesAlta,
        2: this.ValidacionesBaja,
        3: this.ValidacionesCambioContrasena,
        4: this.ValidacionesIniciarSesion,
        5: this.ValidacionesActualizarAvatar,
        6: this.ValidacionesValidarCorreo
    }
    private ValidacionesAlta(){
        ValidationRules
        .ensure((c: Usuarios) => c.Nombre)
        .required()
        .withMessage('Nombre (s) es campo obligatorio.')
        .ensure((c: Usuarios) => c.Apellido)
        .required()
        .withMessage('Apellido (s) es campo obligatorio.')
        .ensure((c: Usuarios) => c.Correo)
        .required()
        .withMessage('Correo electrónico es campo obligatorio.')
        .email().withMessage("Ingrese un formato correcto.")
        .ensure((c: Usuarios) => c.Contrasena)
        .required()
        .withMessage('Contraseña es campo obligatorio.')
        .ensure((c: Usuarios) => c.ConfContrasena)
        .required()
        .withMessage('Confirmar contraseña es campo obligatorio.')
        .ensure((c: Usuarios) => c.Contrasena).equals(this.ConfContrasena)
        .withMessage('Contraseñas no coinciden.')
        .on(Usuarios);
        return new Promise<any>(result => {
            this.Validar(EnumMovimientos.alta)            
            .then(respuesta => {
                return result(respuesta);
            });
        });
    }
    private ValidacionesBaja(){
    
    }
    ValidacionesIniciarSesion(){
        ValidationRules
        .ensure((c: Usuarios) => c.Correo)
        .required()
        .withMessage('Correo electrónico es campo obligatorio.')
        .email().withMessage("Ingrese un formato correcto.")
        .ensure((c: Usuarios) => c.Contrasena)
        .required()
        .withMessage('Contraseña es campo obligatorio.')
        .on(Usuarios);
        return new Promise<any>(result => {      
            this.Validar(EnumMovimientos.iniciarSesion)
            .then(respuesta => {
                return result(respuesta);
            });
        });
    }
    ValidacionesActualizarAvatar(){ 
        ValidationRules
        .ensure((c: Usuarios) => c.Correo)
        .required()
        .withMessage('Correo electrónico es campo obligatorio.')
        .email().withMessage("Ingrese un formato correcto.")
        .ensure((c: Usuarios) => c.Avatar)
        .required()
        .withMessage('Avatar es obligatorio para poder actualizar.')
        .on(Usuarios);
        return new Promise<any>(result => {      
            this.Validar(EnumMovimientos.actualizarAvatar)
            .then(respuesta => {
                return result(respuesta);
            });
        });
    }
    ValidacionesValidarCorreo(){
        ValidationRules
        .ensure((c: Usuarios) => c.Correo)
        .required()
        .withMessage('Correo electrónico es campo obligatorio.')
        .email().withMessage("Ingrese un formato correcto.")        
        .on(Usuarios);
        return new Promise<any>(result => {      
            this.Validar(EnumMovimientos.validarCorreo)
            .then(respuesta => {
                return result(respuesta);
            });
        });
    }

    private ValidacionesCambioContrasena(){
        ValidationRules
        .ensure((c: Usuarios) => c.Contrasena)
        .required()
        .withMessage('advertencia | Contraseña es campo obligatorio.')
        .ensure((c: Usuarios) => c.NuevaContrasena)
        .required()
        .withMessage('advertencia | Nueva contraseña es campo obligatorio.')
        .ensure((c: Usuarios) => c.ConfContrasena)
        .required()
        .withMessage('advertencia | Confirmar contraseña es campo obligatorio.')
        .ensure((c: Usuarios) => c.NuevaContrasena).equals(this.ConfContrasena)
        .withMessage('advertencia | Contraseñas no coinciden.')
        .on(Usuarios);
        this.Validar(EnumMovimientos.cambioContrasena)
    }
    private Validar(mov: EnumMovimientos) {        
        return new Promise<any>(res => {
        this.validacionController.validate()
            .then(result => {
                var guardar:boolean;

                if (result.valid) {
                    guardar = result.valid;
                } else {
                    guardar = result.valid;
                    var resultado = [] = result.results;
                    var hash = true; 
                    resultado.forEach(f => {
                        if (!f.valid && hash) {                
                            this.respuestas.Estatus = EnumRespuestaAPI.ValidacionReglaNegocio;
                            this.respuestas.Mensaje = f.message;
                            this.respuestas.Respuesta = null; 
                            hash = false;
                        }
                    });
                }
                var g: any;
                if (guardar){
                    g =  new Promise<any>(res => {
                        this.Movimientos[mov].call(this)
                        .then(respuesta => {
                            return res(respuesta);
                        });
                    }); 
                } else {
                    g =  this.respuestas;
                }
                return res(g);
            }); 
        });         
    }

    //Metodos Principales
    ValidarCorreo(){
        return new Promise<any>(result => {0
            this.validaciones[EnumValidaciones.validarCorreo].call(this)
            .then(respuesta => {
                return result(respuesta);
            });
        });
    }

    IniciarSesion(){
        return new Promise<any>(result => {0
            this.validaciones[EnumValidaciones.iniciarSesion].call(this)
            .then(respuesta => {
                return result(respuesta);
            });
        });
    }

    Guardar(){ 
        return new Promise<any>(result => {0
            this.validaciones[EnumValidaciones.alta].call(this)
            .then(respuesta => {
                return result(respuesta);
            });
        });
    }
    ActualizarAvatar(){
        return new Promise<any>(result => {0
            this.validaciones[EnumValidaciones.actualizarAvatar].call(this)
            .then(respuesta => {
                return result(respuesta);
            });
        });
    }
    
    CambioContrasena(){
        this.validaciones[EnumValidaciones.cambioContrasena].call(this);
    }
}
 


   // ValidationRules.customRule(
        //     'diferenteDeMenosUno',
        //     (value, obj) => {
        //         console.log(value);
        //       return !(value === -1)
        //     },
        //     `Seleccione un elemento de la lista` 
        // );
        // .ensure((c: Personales) => c.EntidadNacimiento_Id)
        // .required().satisfiesRule('diferenteDeMenosUno')
        // .ensure((c: Personales) => c.Sexo_Id)
        // .required().satisfiesRule('diferenteDeMenosUno')
        // .ensure((c: Personales) => c.EstadoCivil_Id)
        // .required().satisfiesRule('diferenteDeMenosUno')
