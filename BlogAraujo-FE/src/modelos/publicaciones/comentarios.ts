export class Comentarios {
    Descripcion:string;       
    MeGusta: number;
    NoMeGusta: number;
    Correo: string;
    Fecha: string;
    
    constructor(){
        this.Descripcion = '';        
        this.MeGusta = 0;
        this.NoMeGusta = 0;
        this.Correo = '';
        this.Fecha = '';
    }
}