import { autoinject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { ApiPublicaciones } from '../../servicios/webapi/api-publicaciones';
import { Comentarios } from 'modelos/publicaciones/comentarios';
import { Sesion } from 'modelos/seguridad/sesion'; 
import { ValidationController} from 'aurelia-validation';
import { EnumRespuestaAPI } from "../../enumeradores/enum-respuesta-api"; 
import { Respuesta } from '../respuesta/Respuesta';
import { Usuarios } from '../usuarios/Usuarios';
import * as eventos from '../../eventos/eventos';


@autoinject
export class Publicaciones {
    _id:number;
    Avatar: string;
    Categoria: string;
    TituloPublicacion: string;
    Descripcion: string;
    Comentarios: number;
    Vistos: number;
    MeGusta: number;
    NoMeGusta: number;
    Fecha: string;
    Comentario: Comentarios;
    SinComentario: boolean;
    listado: Publicaciones[];
    ListadoComentarios: Comentarios[];
    Categorias: any[];
    respuestas: Respuesta = new Respuesta;
    Usuario: Usuarios;
    ValidarPorID :boolean = false;
    constructor(private ea: EventAggregator, private peticion: ApiPublicaciones,private comentarios: Comentarios, private usuarios: Usuarios, private sesion: Sesion, private validacionController: ValidationController){         
        this._id = 0;
        this.Avatar = '';
        this.Categoria = '';
        this.TituloPublicacion = '';
        this.Descripcion = '';
        this.Comentarios = 0;
        this.Vistos = 0;
        this.MeGusta = 0;
        this.NoMeGusta = 0;
        this.Fecha = '';
        this.Comentario = comentarios;
        this.SinComentario = false;
        this.listado = [];
        this.ListadoComentarios = [];
        this.Usuario = usuarios;
        this.Categorias = [
            "Seleccione",
            "Errores",
            "Rendimiento",
            "Tecnologia",
            "Arquitectura",
            "Proyectos de pruebas"
        ]
        this.ea.subscribe(eventos.ServicioSocket, msg => {
            this._id =  msg._id;
            this.ValidarPorID = true;
            this.ConsultarPorId();
        });
    }
    CambiarMeGusta(publicacionid: any, vista: number){
        this.peticion.CambiarMeGusta(publicacionid);
        if(vista == 1){
            this.listado.forEach(f => {
                if(f._id == publicacionid)
                    f.MeGusta = f.MeGusta + 1;
            });
        } else {
            this.MeGusta = this.MeGusta + 1;
        }
    }
    CambiarNoMeGusta(publicacionid: any, vista: number){
        this.peticion.CambiarNoMeGusta(publicacionid);
        if(vista == 1){
            this.listado.forEach(f => {
                if(f._id == publicacionid)
                    f.NoMeGusta = f.NoMeGusta + 1;
            });
        } else {
            this.NoMeGusta = this.NoMeGusta + 1;
        }
    }
    AgregarComentario(){
        return new Promise<any>(res => {
            var g: any;
            if (this.Comentario.Descripcion == "") {
                this.respuestas.Estatus = EnumRespuestaAPI.ValidacionReglaNegocio;
                this.respuestas.Mensaje = 'El comentario es campo obligatorio.';
                this.respuestas.Respuesta = null; 
                g =  this.respuestas; 
            } else {     
                let obj = {
                    Descripcion: this.Comentario.Descripcion,
                    Correo: this.sesion.Correo,
                    MeGusta: this.Comentario.MeGusta,
                    NoMeGusta: this.Comentario.NoMeGusta
                }
                console.log(obj);
                g = new Promise<any>(result => {
                    this.peticion.AgregarComentario(this._id, obj)
                    .then(respuesta => {
                        return result(respuesta);
                    });
                });
            }
            return res(g);
        });  
    }
    ConsultarPublicaciones(){
        this.peticion.Consultar()
        .then(res => {
            this.listado = res.Respuesta;
        });
    }
    ConsultarPublicacionesPorCategoria(valor: string){
        return new Promise<any>(result => {
            this.peticion.ConsultarPorCategoria(valor)
            .then(res => {
                this.listado = res.Respuesta;
                result (res);
            });
        });


        
    }

    ConsultarPorId(){
        this.peticion.ConsultarPorId(this._id)
        .then(res => {
            if(this.ValidarPorID){
                this.PasarDatosAListado(res.Respuesta);
                this.ValidarPorID = false;
            } else {
                this.PasarDatos(res.Respuesta);
            }
        }); 
    }
    PasarDatosAListado(res:any){
        var PreListado = [];
        PreListado.push(res);
        this.listado.forEach(f => {
            PreListado.push(f);
        });
        this.listado = PreListado;
    }
    private PasarDatos(res: any){
        this.Categoria = res.Categoria;
        this.TituloPublicacion = res.TituloPublicacion;
        this.Descripcion = res.Descripcion;        
        this.Comentarios = res.Comentarios;
        this.Vistos = res.Vistos;
        this.MeGusta = res.MeGusta;
        this.NoMeGusta = res.NoMeGusta;
        this.Fecha = res.Fecha;
        this.usuarios.Avatar = res.Usuario.Avatar;
        this.usuarios.Nombre = res.Usuario.Nombre;
        this.usuarios.Apellido = res.Usuario.Apellido;

        this._id = res._id;
        if(this.Comentarios != 0){
            this.ListadoComentarios = res.ListadoComentarios;
            this.SinComentario = true;
        } else {
            this.SinComentario = false;
        }
    }
    GuardarPublicacion(){   
        return new Promise<any>(res => {
            var g: any;
            if (this.Categoria == "Seleccione") {
                this.respuestas.Estatus = EnumRespuestaAPI.ValidacionReglaNegocio;
                this.respuestas.Mensaje = 'La categoria es campo obligatorio.';
                this.respuestas.Respuesta = null; 
                g =  this.respuestas; 
            } else if (this.TituloPublicacion == "") {                
                this.respuestas.Estatus = EnumRespuestaAPI.ValidacionReglaNegocio;
                this.respuestas.Mensaje = 'El titulo es campo obligatorio.';
                this.respuestas.Respuesta = null; 
                g =  this.respuestas; 
            } else if(this.Descripcion == ""){
                this.respuestas.Estatus = EnumRespuestaAPI.ValidacionReglaNegocio;
                this.respuestas.Mensaje = 'La descripción es campo obligatorio.';
                this.respuestas.Respuesta = null;
                g =  this.respuestas;  
            } else { 
                let obj = {
                    TituloPublicacion :this.TituloPublicacion,
                    Descripcion:this.Descripcion,
                    Comentarios :0,
                    Vistos:0,
                    MeGusta :0,
                    NoMeGusta :0,
                    Correo :this.sesion.Correo, 
                    Categoria :this.Categoria
                }
                console.log(obj);
                g = new Promise<any>(result => {
                    this.peticion.Guardar(obj)
                    .then(respuesta => {
                        return result(respuesta);
                    });
                });
            }
            return res(g);
        });  
    }
}