import { autoinject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { Router } from 'aurelia-router';
import * as eventos from './eventos/eventos';
import { EnumVistas } from './enumeradores/enum-vistas';
import { Seguridad } from './modelos/seguridad/seguridad'; 
import { Sesion } from './modelos/seguridad/sesion';


@autoinject
export class App {
    Vistas: string;
    VistasModelos: string;

    migas: any[] = [];
    flujo:number= 1;
    MigasdePagan: any[] = [
        {nombre: "Publicación", icono: false, flujo: 1},
        {nombre: "Publicación,Detalle Publicación", icono: true, flujo: 2},
        {nombre: "Publicación,Nueva Publicación", icono: true, flujo: 3}
    ];
    constructor(private ea: EventAggregator, private sesion: Sesion, private seguridad: Seguridad){
        
        
        // this.MigasDePan();
        this.Subscribe();
        this.AreaTrabajo();
    }
    
    Subscribe(){
        this.ea.subscribeOnce(eventos.SesionEstablecidaEvent, e => {
            if (this.sesion.Token != ""){
                if (e.valor != 1) //Quitar Menu Derecho
                    this.ea.publish(new eventos.QuitarMenuDerechoUsuarioLogeado());
                    this.ea.publish(new eventos.TokenValido());
            } else {
                this.ea.publish(new eventos.TokenNoValido());
            }
        });
        this.ea.subscribe(eventos.VistaTrabajo, res => {
            this.Vistas = res.vista.vista;
            this.VistasModelos = res.vista.modelo;
        });

    }
    
    AreaTrabajo(){ 
        this.Vistas = EnumVistas.vistaPublicaciones["vista"];
        this.VistasModelos = EnumVistas.vistaPublicaciones["modelo"];
    }

    MigasDePan(){
        this.migas = [];
        this.MigasdePagan.forEach(s => {
            if(s.flujo === this.flujo){
                var arrNom = s.nombre.split(',');
                arrNom.forEach(nom =>{
                    let objM = {nombre:nom, icono:s.icono}
                    this.migas.push(objM); 
                }); 
            }
        });
    }
}

String.prototype["format"] = function () {
    var base = this;
    for (var ndx = 0; ndx < arguments.length; ndx++) {
        var regexp = new RegExp("\\{" + ndx.toString() + "}", "gi");
        base = base.replace(regexp, arguments[ndx]);
    }
    return base;
}
