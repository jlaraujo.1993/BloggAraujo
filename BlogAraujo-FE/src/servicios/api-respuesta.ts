import { autoinject } from 'aurelia-framework';
import { EnumRespuestaAPI } from "../enumeradores/enum-respuesta-api";
import { EventAggregator } from 'aurelia-event-aggregator';
import * as eventos from '../eventos/eventos';

@autoinject
export class ApiRespuesta {
    constructor(public ea: EventAggregator){

    }
    public ProcesarRespuesta(respuesta): any { 
        var resultado; 
         
        switch (respuesta.Estatus)
        {
            case EnumRespuestaAPI.Aceptado:{
                if(respuesta.Mensaje != ''){
                    this.ea.publish(new eventos.MostrarMensaje('exito',respuesta.Mensaje));
                    resultado = respuesta.Respuesta;
                } else {
                    resultado = respuesta.Respuesta;
                }
                break;
            }
            case EnumRespuestaAPI.NoEncontrado:{
                if(respuesta.Mensaje != ''){
                    this.ea.publish(new eventos.MostrarMensaje('peligro',respuesta.Mensaje));
                    resultado = respuesta.Respuesta;
                } else {
                    resultado = respuesta.Respuesta;
                }
                break; 
            }
            case EnumRespuestaAPI.ErrorInterno:{
                if(respuesta.Mensaje != ''){
                    this.ea.publish(new eventos.MostrarMensaje('peligro',respuesta.Mensaje));
                    resultado = respuesta.Respuesta;
                } else { 
                    resultado = respuesta.Respuesta;
                }
                break; 
            }
            case EnumRespuestaAPI.ValidacionReglaNegocio:{
                if(respuesta.Mensaje != ''){
                    this.ea.publish(new eventos.MostrarMensaje('advertencia',respuesta.Mensaje));
                    resultado = respuesta.Respuesta;
                } else {
                    resultado = respuesta.Respuesta;
                }
                break; 
            }
            case EnumRespuestaAPI.NoPermitido:{
                if(respuesta.Mensaje != ''){
                    this.ea.publish(new eventos.MostrarMensaje('advertencia',respuesta.Mensaje));
                    resultado = respuesta.Respuesta;
                } else {
                    resultado = respuesta.Respuesta;
                }
                break;
            }
        }
        return resultado;
    }
}