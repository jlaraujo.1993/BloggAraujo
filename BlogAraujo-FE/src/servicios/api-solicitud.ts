import { HttpClient, json, RequestInit } from 'aurelia-fetch-client';
import { autoinject } from "aurelia-framework";

//import { Sesion } from './sesion';

export class Constantes {
    static token = 's-token';
    static pais_id = 's-paisid';
}
@autoinject
export class ApiSolicitud extends HttpClient {
    Respuesta: any;
    RespuestaApi: any;

    get(url: string): Promise<any> {
        let self = this;
        return new Promise<any>((res, err) => {
            self.fetch(url)
                .then(respuesta =>  respuesta.json())
                .then(respuesta =>  res(respuesta))
                .catch(error => err(error));
        });        
    }

    post(url: string, objeto: any): Promise<any> {
        let self = this;
        return new Promise<any>((res, err) => {
            let init: RequestInit = { };
            init.method = "post";
            init.body = json(objeto);            
            self.fetch(url, init)
            .then(respuesta => respuesta.json())
            .then(respuesta => res(respuesta))
            .catch(error => err(error)); 
        });
    }
    put(url: string, objeto: any): Promise<any> {
        let self = this;
        return new Promise<any>((res, err) => {
            let init: RequestInit = { };
            init.method = "put";
            init.body = json(objeto);            
            self.fetch(url, init)
            .then(respuesta => respuesta.json())
            .then(respuesta => res(respuesta))
            .catch(error => err(error)); 
        });
    }
}