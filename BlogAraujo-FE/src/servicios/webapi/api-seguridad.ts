import { autoinject } from "aurelia-framework";
import environment from './../../environment';
import { Sesion } from '../../modelos/seguridad/sesion'; 
import { ApiSolicitud } from '../api-solicitud';

class ApiSeguridadMethods {
    constructor(private apiBase: string) {
        this.apiBase = this.apiBase + "{0}";
    }    
    VerificaToekn() {
        return this.apiBase["format"]('/verificatoken');
    };
    IniciarSesion() {
        return this.apiBase["format"]('/iniciarsesion');
    };
    CerrarSesion() {
        return this.apiBase["format"]('/cerrarsesion');
    };
    Login() {
        return this.apiBase["format"]('/login');
    };
}

@autoinject
export class ApiSeguridad {
    apis: ApiSeguridadMethods;

    constructor(private sesion: Sesion, private api: ApiSolicitud) {
    this.apis = new ApiSeguridadMethods(environment.apiUrl.seguridad);
        
    }

    Login(obj: any): Promise<any> { 
        return new Promise<any>(result => {0
            this.api.post(this.apis.Login() , obj)
            .then(respuesta => {
                return result(respuesta);
            });
        }); 
    }

    VerificarToken(): Promise<any> { 
        return new Promise<any>(result => {0
            this.api.post(this.apis.VerificaToekn() , this.sesion)
            .then(respuesta => {
                return result(respuesta);
            });
        }); 
    }

    IniciarSesion(): Promise<any> {
        return new Promise<any>(result => {0
            this.api.post(this.apis.IniciarSesion(), this.sesion)
            .then(respuesta => {
                return result(respuesta);                    
            });
        }); 
    }

    CerrarSesion(): Promise<any> {
        return new Promise<any>(result => {0
            this.api.post(this.apis.CerrarSesion(), this.sesion)
            .then(respuesta => {
                return result(respuesta);                    
            });
        }); 
    }
//   DesactivarToken() {

//     this.api.get(ambiente.apiUrl.sesion.concat('DesactivarToken'))
//       .then(respuesta => {
//         location.href = ambiente.webUrl.login;
//       })
//       .catch(error => {

//       });
//   }

//   IdentificarUsuario(nombreUsuario: string): Promise<any> {
//     if (nombreUsuario) {
//       let self = this;
//       return new Promise<any>((resultado, err) => {
//         self.api.get(ambiente.apiUrl.sesion.concat("obtenerLlave/" + nombreUsuario.replace(/\./gi, "_")))
//           .then(respuesta => resultado(self.ProcesarResultado(respuesta)))
//           .catch(error => resultado(self.ProcesarError(error)));
//       });
//     }
//     return null;
//   }



//   ProcesarResultado(respuesta: any): any {
//     let resultado = {
//       datos: respuesta.Respuesta || {},
//       mensaje: respuesta.Mensaje
//     };
//     return resultado;
//   }

//   ProcesarError(respuesta: any): any {
//     respuesta.status = respuesta.status || respuesta.Codigo;
//     let resultado = {};
//     if (respuesta.status == 404) {
//       resultado = { datos: [], mensaje: 'No hay conexión.' };
//     } else if (respuesta.status >= 500 && respuesta.status < 1000) {
//       resultado = { datos: [], mensaje: 'Error interno.' };
//     } else if (respuesta.status == 1002) {
//       resultado = { datos: null, mensaje: respuesta.Mensaje };
//     } else {
//       resultado = { datos: [], mensaje: 'Error no controlado.' };
//     }
//     return resultado;
//   }

//   cambiarContrasena(nombreUsuario: string, contrasenaActual: string, contrasenaNva: string): Promise<any> {
//     if (nombreUsuario) {
//       let self = this;
//       return new Promise<any>((resultado, err) => {
//         self.api.post(ambiente.apiUrl.sesion.concat("cambiarcontrasena/contrasena"), { nombreUsuario, contrasenaActual, contrasenaNva })
//           .then(respuesta => resultado(self.ProcesarResultado(respuesta)))
//           .catch(error => resultado(self.ProcesarError(error)));
//       });
//     }
//     return null;
//   }
}