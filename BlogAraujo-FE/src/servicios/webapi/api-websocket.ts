import { autoinject } from "aurelia-framework";
import environment from '../../environment';
import { EventAggregator } from 'aurelia-event-aggregator';
import { Sesion } from '../../modelos/seguridad/sesion';
import * as eventos from '../../eventos/eventos';

@autoinject
export class ApiWebSocket {
    socket: WebSocket;

    constructor(private ea: EventAggregator, private sesion: Sesion) {

    }

    private onMessage(ev: MessageEvent){ 
        this.ea.publish(new eventos.ServicioSocket(ev.data)); 
    }

    EnviarMensaje(pubicacionid: any){
        if (this.socket && this.socket.readyState == this.socket.OPEN) { 
            //let obj = {CorreoOrigen: this.sesion.Correo, Mensaje:pubicacionid}
            //this.socket.send(JSON.stringify(obj));
            this.socket.send(pubicacionid);
          } else if (this.socket && this.socket.readyState == this.socket.CLOSED) {
            this.conectar();
          }
    }

    conectar(){ 
        this.socket = new WebSocket(environment.socket.servicioSokect+this.sesion.Correo);
        var self = this;
        this.socket.onmessage = (ev: MessageEvent) => {
           this.onMessage(ev);
         };
    }
    cerrar() {
        if (this.socket && this.socket.readyState == this.socket.OPEN)
            this.socket.close();
    }
}
