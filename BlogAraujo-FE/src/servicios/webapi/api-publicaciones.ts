import { autoinject } from "aurelia-framework";
import environment from '../../environment';
import { ApiSolicitud } from '../api-solicitud';

class ApiPublicacionesMethods {
    constructor(private apiBase: string) {
        this.apiBase = this.apiBase + "{0}";
    };
    Guardar() {
        return this.apiBase["format"]('');
    };
    Consultar() {
        return this.apiBase["format"]('');
    };
    AgregarComentario(publicacionid) {
        return this.apiBase["format"]('/'+publicacionid);
    };
    CambiarMeGusta(publicacionid) {
        return this.apiBase["format"]('/megusta/'+publicacionid);
    };
    CambiarNoMeGusta(publicacionid) {
        return this.apiBase["format"]('/nomegusta/'+publicacionid);
    };
    ConsultarPorId(PublicacionId) {
        return this.apiBase["format"]('/'+PublicacionId);
    };
    ConsultarPorCategoria(categoria:string) {
        return this.apiBase["format"]('/categoria/'+categoria);
    };

    
}

@autoinject
export class ApiPublicaciones {
    apis: ApiPublicacionesMethods;

    constructor(/*public sesion: Sesion,*/ private api: ApiSolicitud) {
        this.apis = new ApiPublicacionesMethods(environment.apiUrl.publicaciones);
    }
    
    public CambiarMeGusta(publicacionid: any){
        return new Promise<any>(result => {
            this.api.put(this.apis.CambiarMeGusta(publicacionid),{})
            .then(respuesta => {
                return result(respuesta);
            });
        });
    }
    public CambiarNoMeGusta(publicacionid: any){
        return new Promise<any>(result => {
            this.api.put(this.apis.CambiarNoMeGusta(publicacionid),{})
            .then(respuesta => {
                return result(respuesta);
            });
        });
    }
    public AgregarComentario(publicacionid: any, comentario: any){
        return new Promise<any>(result => {
            this.api.put(this.apis.AgregarComentario(publicacionid), comentario)
            .then(respuesta => {
                return result(respuesta);
            });
        });
    }
    public Guardar(publicacion: any){
        return new Promise<any>(result => {
            this.api.post(this.apis.Guardar(), publicacion)
            .then(respuesta => {
                return result(respuesta);
            });
        });
    }
    public Consultar(){
        return new Promise<any>(result => {
            this.api.get(this.apis.Consultar())
            .then(respuesta => {
                return result(respuesta);
            });
        });
    }
    public ConsultarPorCategoria(categoria: string){
        return new Promise<any>(result => {
            this.api.get(this.apis.ConsultarPorCategoria(categoria))
            .then(respuesta => {
                return result(respuesta);
            });
        });
    }
    public ConsultarPorId(PublicacionId){
        return new Promise<any>(result => {
            this.api.get(this.apis.ConsultarPorId(PublicacionId))
            .then(respuesta => {
                return result(respuesta);
            });
        });
    }
}
