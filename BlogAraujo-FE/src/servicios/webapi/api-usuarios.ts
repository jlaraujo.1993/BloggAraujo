import { autoinject } from "aurelia-framework";
import environment from '../../environment';
import { ApiSolicitud } from '../api-solicitud';

class ApiUsuariosMethods {
    constructor(private apiBase: string) {
        this.apiBase = this.apiBase + "{0}";
    }
    Consultar() {
        return this.apiBase["format"]('');
    };
    Guardar() {
        return this.apiBase["format"]('');
    };
    ActualizarAvatar(){
        return this.apiBase["format"]('');
    };
    ValidarCorreo(){
        return this.apiBase["format"]('/validarcorreo');
    }
}

@autoinject
export class ApiUsuario {
    apis: ApiUsuariosMethods;

    constructor(private api: ApiSolicitud) {
        this.apis = new ApiUsuariosMethods(environment.apiUrl.usuarios);
    }

    ValidarCorreo(Correo: any){
        return new Promise<any>(result => {0
            this.api.post(this.apis.ValidarCorreo(), Correo)
            .then(respuesta => {
                return result(respuesta);                    
            });
        });
    }
    public ActualizarAvatar(usuario: any){ 
        return new Promise<any>(result => {0
            this.api.put(this.apis.ActualizarAvatar(), usuario)
            .then(respuesta => {
                return result(respuesta);                    
            });
        });
    }
    public Guardar(usuario: any){ 
        return new Promise<any>(result => {0
            this.api.post(this.apis.Guardar(), usuario)
            .then(respuesta => {
                return result(respuesta);                    
            });
        });
    }
    public Consultar(){ 
        return new Promise<any>(result => {
            this.api.get(this.apis.Consultar())
            .then(respuesta => {
                return result(respuesta);
            });
        });
    }
}