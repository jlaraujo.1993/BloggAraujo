import {autoinject} from 'aurelia-framework';

@autoinject
export class LimpiarCustomAttribute {
  constructor(private element: Element) {
    var modelo = this;
    let padre = element.parentElement; 

    var div_borrar = document.createElement("span");
    padre.appendChild(div_borrar);
    div_borrar.className = "reset";  

    div_borrar.addEventListener("click", function () {
      if (!element.hasAttribute("disabled")) {
        let ValidarPadre = element.parentElement;
        var property = element.attributes["value.bind"];

        if (property["value"].indexOf(".") > -1) {
          var QuitarValidate = property["value"].split('&');
          var partes = QuitarValidate[0].split('.'); 
          var obj = element["au"].limpiar.scope.bindingContext[partes[0]];
          for (let ndx = 1; ndx < partes.length - 1; ndx++) {
              obj = obj[partes[ndx]];
          }
          obj[partes[partes.length - 1]] = "";
        } else {          
          element["au"].limpiar.scope.bindingContext[property["value"]] = "";
        }
      }
    });
  }  
}
