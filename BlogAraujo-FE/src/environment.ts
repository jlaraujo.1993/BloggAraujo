export default {
  debug: false,
  testing: false,
  apiUrl: {
    pruebas: 'http://www.mocky.io/v2/5b53b1ee2f00004f00061900',
    publicaciones: 'http://localhost:9001/api/publicaciones',
    usuarios: 'http://localhost:9001/api/usuario',
    seguridad: 'http://localhost:9001/api/seguridad',
  },
  webUrl: {
    login: 'http://localhost:9000/',
  },
  socket: {
    servicioSokect: 'ws://localhost:1337/',
  }
};
