export enum EnumValidaciones { 
    alta = 1,
    baja = 2,
    cambioContrasena = 3,
    iniciarSesion = 4,
    actualizarAvatar = 5,
    validarCorreo = 6
}