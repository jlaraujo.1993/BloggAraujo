export enum EnumVistas { 
    vistaNinguna = <any>{vista: '', modelo: ''},
    vistaRegistroUsuarios = <any>{vista: '../../vistas/usuarios/registro-usuarios.html', modelo: '../../vistas/usuarios/registro-usuarios'},
    vistaPerfilUsuarios = <any>{vista: '../../vistas/usuarios/perfil-usuarios.html', modelo: '../../vistas/usuarios/perfil-usuarios'},
    vistaIniciarSesion = <any>{vista: '../../vistas/usuarios/iniciar-sesion.html', modelo: '../../vistas/usuarios/iniciar-sesion'},
    vistaPublicaciones = <any>{vista: './vistas/publicaciones/listado-publicaciones.html', modelo: './vistas/publicaciones/listado-publicaciones'},
    vistaDetallePublicacion = <any>{vista: './vistas/publicaciones/detalle-publicacion.html', modelo: './vistas/publicaciones/detalle-publicacion'},
    vistaNuevaPublicacion = <any>{vista: './vistas/publicaciones/nueva-publicacion.html', modelo: './vistas/publicaciones/nueva-publicacion'}
} 