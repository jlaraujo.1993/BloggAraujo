export enum EnumRespuestaAPI {
    Aceptado = 200,
    NoEncontrado = 404,
    ErrorInterno = 503,
    ValidacionReglaNegocio = 1002,
    NoPermitido = 409
}