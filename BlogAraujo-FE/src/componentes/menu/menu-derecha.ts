import { autoinject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import * as eventos from '../../eventos/eventos';
import { EnumVistas } from '../../enumeradores/enum-vistas';

//import { Sesion } from 'sd/seguridad/sesion';
//import { ApiRH } from './../servicios/webapi/api-rh';
//import { ApiSeguridad } from 'sd/seguridad/api-seguridad';

@autoinject
export class MenuDerecha {
  activo: boolean = false;
  // //apiRH: ApiRH;
  // inicialesUsuario: string;
  // visible: boolean = true;

  Vista: string;
  VistaModelo: string;

  constructor(private ea: EventAggregator) {

    this.ea.subscribe(eventos.MostrarQuitarMenuDerecho, res => {      
      this.activo = res.Mostrar;
      if(this.activo){
        this.Vista = res.vista.vista
        this.VistaModelo = res.vista.modelo;
      }
    });
    
  }
}
