import {autoinject} from 'aurelia-framework';
import { EventAggregator} from 'aurelia-event-aggregator';
import { Sesion } from '../../modelos/seguridad/sesion';
import * as eventos from '../../eventos/eventos';
import { EnumVistas } from '../../enumeradores/enum-vistas';
import { Publicaciones } from '../../modelos/publicaciones/publicaciones';
import { ApiRespuesta } from '../../servicios/api-respuesta';
import { EnumRespuestaAPI } from "../../enumeradores/enum-respuesta-api"; ;


@autoinject
export class MenuArriba {
  CerrarMenuDerecho: boolean;
  agregarCategoria: boolean;
  Inicio: boolean = true;  
  logeado: boolean = false;
  Avatar: string;
  constructor(private sesion: Sesion, private ea: EventAggregator, public publicaciones: Publicaciones, private apiRespuesta: ApiRespuesta) {
    this.Subscribe();
  }
  Subscribe(){
    this.ea.subscribe(eventos.QuitarMenuDerechoUsuarioLogeado, msg => {
      this.ea.publish(new eventos.MostrarQuitarMenuDerecho(false, EnumVistas.vistaNinguna));
        this.logeado = true;
        this.CerrarMenuDerecho = false;
        this.agregarCategoria = true;
    });
    this.ea.subscribe(eventos.TokenValido, msg => {
      this.Avatar = this.sesion.Avatar;
      this.agregarCategoria = true; 
      this.Inicio = false;
      this.logeado = true;
    });  
    this.ea.subscribe(eventos.TokenNoValido, msg => {
      this.agregarCategoria = false;
      this.Inicio = true;
      this.CerrarMenuDerecho = false;
      this.logeado = false;
    });      
  }
  ConsultarTipoCategoria(valor: string){
    console.log(this.publicaciones.listado.length);
    this.publicaciones.ConsultarPublicacionesPorCategoria(valor)
        .then(respuesta => {
            this.apiRespuesta.ProcesarRespuesta(respuesta);
            if (respuesta.Estatus == EnumRespuestaAPI.Aceptado){
            }
        }); 
  }
  Todas(){
    this.publicaciones.ConsultarPublicaciones();
  }
  Home(){    
    this.sesion.PrimeraVes = true;
    this.ea.publish(new eventos.VistaTrabajo(EnumVistas.vistaPublicaciones));
  }
	QuitarMenuDerecho() {
    this.ea.publish(new eventos.MostrarQuitarMenuDerecho(false,EnumVistas.vistaNinguna));
    if (this.sesion.Token == ""){
      this.CerrarMenuDerecho = false;
      this.Inicio = true;
    } else {
      this.logeado = true;
      this.CerrarMenuDerecho = false;
    }


  }
  AgregarPublicacion(){ 
      this.ea.publish(new eventos.VistaTrabajo(EnumVistas.vistaNuevaPublicacion));
  }

  MostrarPerfil(){
    this.logeado = false;
    this.CerrarMenuDerecho = true;
    this.ea.publish(new eventos.MostrarQuitarMenuDerecho(true,EnumVistas.vistaPerfilUsuarios));
  }
 

	Registrarse() {
    this.CerrarMenuDerecho = true;
    this.Inicio = false;
		this.ea.publish(new eventos.MostrarQuitarMenuDerecho(true, EnumVistas.vistaRegistroUsuarios));
  }
  IniciarSesion(){
    this.CerrarMenuDerecho = true;
    this.Inicio = false;
		this.ea.publish(new eventos.MostrarQuitarMenuDerecho(true, EnumVistas.vistaIniciarSesion));
  }
}

