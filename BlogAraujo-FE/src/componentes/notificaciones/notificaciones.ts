import { EventAggregator } from 'aurelia-event-aggregator';
import {autoinject} from 'aurelia-framework';

//import { Sesion } from 'sd/seguridad/sesion';
//import { SesionEstablecidaEvent } from 'sd/seguridad/contralor';
//import ambiente from './../environment';
@autoinject
export class Notificaciones {
  activo: boolean = false;
  notificaciones: any[] = [];
  //ws: WebSocket;

  constructor(/*private sesion: Sesion,*/ private ea: EventAggregator) {
    /*let self = this;
    this.ea.subscribe(SesionEstablecidaEvent, e => {
      document.cookie = "s-token=" + self.sesion.Token.replace(/=/gi, "|") + "; path=/";
      self.ws = new WebSocket(ambiente.socket.notificaciones + self.sesion.NombreUsuario.replace(/\./gi, "_"));
      self.ws.onmessage = (ev: MessageEvent) => {
        var nots = JSON.parse(ev.data);

        //
        
        self.notificaciones.push(nots);
        console.log(self.notificaciones);
      };
    });*/
  }

  get contador(): number {
    return this.notificaciones.length > 99 ? 99 : this.notificaciones.length;
  }

  get mostrarContador(): boolean {
    return this.contador > 0;
  }

  toggleNot() {
    this.activo = !this.activo;
  }
}
