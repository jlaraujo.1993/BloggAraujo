import { EventAggregator } from 'aurelia-event-aggregator';
import { autoinject } from 'aurelia-framework';
import * as eventos from '../../eventos/eventos';

@autoinject
export class Mensaje{
    tipoAlerta: string;
    mostrarAlerta: string;
    mensajeAlerta: string;
    posicionAlerta: string;
    tiempoMensaje: number = 5000;
    cerrarMensajeHandle: any;
    constructor(private ea: EventAggregator){
        this.InicializarVariablesParaAlerta(); 
    }

    MandarAlertas(claseAlerta, mensaje) {
        this.alerta(claseAlerta, 'mostrar', mensaje, 'derecha-arriba');

        if (this.cerrarMensajeHandle !== undefined) {
            window.clearTimeout(this.cerrarMensajeHandle);
        }

        this.cerrarMensajeHandle = setTimeout(() => {
            this.cerrarAlerta();
        }, this.tiempoMensaje);
    }
    InicializarVariablesParaAlerta() {
        this.ea.subscribe(eventos.MostrarMensaje, res => {
            this.MandarAlertas(res.claseAlerta, res.mensaje);
        });
        this.tipoAlerta = 'normal';
        this.mostrarAlerta = 'ocultar';
        this.mensajeAlerta = '';
        this.posicionAlerta = 'derecha-arriba';
    }

    alerta(tipo: string,  mostrar: string, mensaje: string, posicion: string) {
        document.getElementById("divAlerta").classList.remove("bounceOut");
        document.getElementById("divAlerta").classList.add("bounceIn");
        setTimeout(() => {
            this.tipoAlerta = tipo;
            this.mensajeAlerta = mensaje;
            this.posicionAlerta = posicion;
            this.mostrarAlerta = mostrar;
        }, 100);
    }

    cerrarAlerta() {
        document.getElementById("divAlerta").classList.add("bounceOut");
        setTimeout(() => {
            this.mostrarAlerta = 'ocultar';
            this.tipoAlerta = 'normal';
            this.mensajeAlerta = '';
            this.posicionAlerta = 'derecha-arriba';
        }, 1000);
    }
}